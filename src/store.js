import Vue from 'vue'
import Vuex from 'vuex'
import Chance from 'chance'

Vue.use(Vuex);

const state = {
  providedImg: "",
  receivedImgs: [],
  sessionId: new Chance().guid(),
};

const actions = {};

const mutations = {
  SET_PROVIDED_IMG(state, img) {
    state.providedImg = img;
  },
  SET_IMG_IN_PROGRESS(state, id) {
    state.receivedImgs.push({
      id: id,
      url: ''
    });
  },
  SET_IMG_DONE(state, payload) {
    let index = state.receivedImgs.map(it => it.id).indexOf(payload.id);
    state.receivedImgs.splice(index, 1, {
      id: payload.id,
      url: payload.url
    });
  },
  DELETE_IMG_IN_PROGRESS(state, id) {
    let index = state.receivedImgs.map(it => it.id).indexOf(id);
    state.receivedImgs.splice(index, 1)
  }
};

export default new Vuex.Store({
  state,
  mutations,
  actions
})
