// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import VueResource from "vue-resource";
import Toasted from 'vue-toasted';
import store from './store'

import 'font-awesome/css/font-awesome.min.css'

Vue.use(VueResource);
Vue.use(Toasted);

Vue.config.productionTip = false;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  store: store
});

import App from './App'


//TODO: Move to separate file

let defaultActions = [
  {
    text : 'CLOSE',
    onClick : (e, toastObject) => {
      toastObject.goAway(0);
    }
  }
];

Vue.toasted.register('error',
  (payload) => {
    return payload;
  }, {
    type: 'error',
    action: defaultActions,
    duration: 5000,
    position: 'bottom-center'
  }
);

Vue.toasted.register('info',
  (payload) => {
    return payload;
  }, {
    type: 'info',
    action: defaultActions,
    duration: 2000,
    position: 'bottom-center'
  }
);

Vue.toasted.register('success',
  (payload) => {
    return payload;
  }, {
    type: 'success',
    action: defaultActions,
    duration: 3000,
    position: 'bottom-center'
  }
);
