# budziol-front

> Frontend for budziol project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


### Notes to future me

Holy fuck is this project messy.

If you want to build it, then *use older npm (I'm using 6.17.1 atm)*

Also, the deploy process.. What was I thinking. Just so you don't have to remember what `deploy.sh` does, it:
* Builds locally
* Pushes `dist` to some god forsaken github project (why? How the fuck should I know?)
* It should get published shortly.

So *just run `deploy.sh` locally.*
